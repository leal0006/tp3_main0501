using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouvement : MonoBehaviour
{
    float rotationSpeed = 50f; // Vitesse de rotation
    float speed = 2.5f; // Vitesse de d�placement
    float rotationY;
    public float forceMoufle = 500f;
    public ArticulationBody Moufle;
    public Camera[] cameras;  // Un tableau pour contenir vos 4 cam�ras
    private int currentCameraIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            cameras[i].enabled = (i == 0);
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Si la fl�che gauche est press�e, rotation n�gative sur l'axe Y
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.Rotate(0, 0, -rotationSpeed * Time.deltaTime);
        }

        // Si la fl�che droite est press�e, rotation positive sur l'axe Y
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.Rotate(0, 0, rotationSpeed * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(0, speed * Time.deltaTime, 0);
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            if (cameras.Length > 0) // V�rifier que le tableau de cam�ras n'est pas vide
            {
                cameras[currentCameraIndex].enabled = false;
                currentCameraIndex = (currentCameraIndex + 1) % cameras.Length;
                cameras[currentCameraIndex].enabled = true;
            }
        }
    }
}
